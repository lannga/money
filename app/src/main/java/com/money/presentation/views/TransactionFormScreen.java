package com.money.presentation.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.money.R;
import com.money.presentation.interfaces.ITransactionList;

public class TransactionFormScreen extends AppCompatActivity implements ITransactionList.View {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_form_screen);
    }

    @Override
    public void renderList() {

    }
}
