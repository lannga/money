package com.money.presentation.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.money.R;
import com.money.presentation.interfaces.IStatisticList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainScreen extends AppCompatActivity implements IStatisticList.View {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.cat_btn)
    public void goToCategoryList(){
        Intent intent = new Intent(this, CategoryListScreen.class);
        startActivity(intent);
    }

    @Override
    public void renderList() {

    }
}
