package com.money.presentation.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.money.R;
import com.money.presentation.interfaces.ICategoryList;

import butterknife.OnClick;

public class CategoryListScreen extends AppCompatActivity implements ICategoryList.View {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list_screen);
    }

    @OnClick(R.id.cat_create_btn)
    public void goToCategoryForm(){
        Intent intent = new Intent(this, CategoryFormScreen.class);
        startActivity(intent);
    }

    @Override
    public void renderList() {

    }
}
