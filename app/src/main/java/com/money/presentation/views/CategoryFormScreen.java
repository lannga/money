package com.money.presentation.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.money.R;
import com.money.presentation.interfaces.ICategoryForm;

public class CategoryFormScreen extends AppCompatActivity implements ICategoryForm.View{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_form_screen);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT);
    }

    @Override
    public void goToCategoryList() {
        finish();
    }
}
