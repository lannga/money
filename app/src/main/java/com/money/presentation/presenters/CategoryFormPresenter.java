package com.money.presentation.presenters;

import android.content.Context;
import android.widget.EditText;

import com.money.data.dao.AppDatabase;
import com.money.data.dao.CategoryAccess;
import com.money.data.entity.DCategory;
import com.money.presentation.interfaces.ICategoryForm;
import com.money.presentation.modals.Category;
import com.money.utils.Format;

import java.util.LinkedList;
import java.util.List;

public class CategoryFormPresenter implements ICategoryForm.Presenter {

    private ICategoryForm.View view;
    private CategoryAccess categoryAccess;


    public CategoryFormPresenter(ICategoryForm.View view, Context context) {
        this.view = view;
        categoryAccess = AppDatabase.getInstance(context).categoryAccess();
    }


    @Override
    public void validate(String type, int icon, String name, final Category root) {

        if (!validateEmpty(name)) return;
        if (!validateExisted(type, name, root)) return;
        insert(type, icon, name, root);

    }

    public boolean validateEmpty(String name) {
        if (name == null) {
            view.showError("Chưa nhập tên");
            return false;
        }
        return true;
    }

    public boolean validateExisted(String strType, final String name, final Category root) {
        final int type = Format.type(strType);
        final List<DCategory> results = new LinkedList<>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (root == null) {
                    results.addAll(categoryAccess.checkExisted(type, name));
                } else {
                    results.addAll(categoryAccess.checkExisted(root.getId(), type, name));
                }
            }
        });
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (results.size() > 0) {
            view.showError("DCategory đã tồn tại");
            return false;
        }
        return true;
    }

    @Override
    public void insert(String strType, int icon, String name, Category root) {
        DCategory category = new DCategory(name, icon, root.getId(), Format.type(strType));
        categoryAccess.insert(category);
    }
}
