package com.money.presentation.presenters;

import android.widget.EditText;

import com.money.presentation.interfaces.ITransactionForm;

public class TransactionFormPresenter implements ITransactionForm.Presenter {
    @Override
    public void validate(EditText... editTex) {

    }

    @Override
    public void insert() {

    }
}
