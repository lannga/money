package com.money.presentation.modals;

public class Statistic {

    private Category category;
    private int total;
    private String time;

    public Statistic(Category category, int total, String time) {
        this.category = category;
        this.total = total;
        this.time = time;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
