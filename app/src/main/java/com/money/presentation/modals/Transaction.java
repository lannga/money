package com.money.presentation.modals;

public class Transaction {

    private String id;
    private Category category;
    private int amount;
    private long time;
    private String note;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Transaction(String id, Category category, int amount, long time, String note) {
        this.id = id;
        this.category = category;
        this.amount = amount;
        this.time = time;
        this.note = note;
    }
}
