package com.money.presentation.modals;

public class Category {
    private String id;
    private String name;
    private int icon;
    private String rootId;
    private int type;

    public Category(String id, String name, int icon, String rootId, int type) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.rootId = rootId;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
