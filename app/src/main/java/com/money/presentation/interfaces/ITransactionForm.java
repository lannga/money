package com.money.presentation.interfaces;

import android.widget.EditText;

public interface ITransactionForm {
    interface View{
        void showError(String message);
    }

    interface Presenter{
        void validate(EditText... editTex);
        void insert();
    }
}
