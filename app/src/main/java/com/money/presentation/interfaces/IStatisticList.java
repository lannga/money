package com.money.presentation.interfaces;

public interface IStatisticList {
    interface View{
        void renderList();
    }

    interface Presenter{

    }
}
