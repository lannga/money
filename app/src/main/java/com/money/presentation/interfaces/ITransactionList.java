package com.money.presentation.interfaces;

public interface ITransactionList {
    interface View{
        void renderList();
    }

    interface Presenter{

    }
}
