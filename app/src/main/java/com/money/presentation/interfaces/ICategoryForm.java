package com.money.presentation.interfaces;

import android.widget.EditText;

import com.money.presentation.modals.Category;

public interface ICategoryForm {
    interface View{
        void showError(String message);
        void goToCategoryList();
    }

    interface Presenter{
        void validate(String type,int icon, String name, Category root);
        void insert(String type,int icon, String name, Category root);
    }
}
