package com.money.presentation.interfaces;

public interface ICategoryList {
    interface View{
        void renderList();
    }

    interface Presenter{
        void getList();
    }
}
