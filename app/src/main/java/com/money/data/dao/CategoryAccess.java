package com.money.data.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.money.data.entity.DCategory;

import java.util.List;

@Dao
public interface CategoryAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DCategory... categories);

    @Update
    void update(DCategory... categories);

    @Delete
    void delete(DCategory... categories);


    @Query("SELECT * FROM DCategory")
    List<DCategory> getAll();

    @Query("SELECT * FROM DCategory WHERE rootId IS NULL")
    List<DCategory> getRoot();

    @Query("SELECT * FROM DCategory WHERE rootId =:catId")
    List<DCategory> getByRootId(String catId);

    @Query("SELECT * FROM DCategory WHERE rootId =:rootId AND type =:type AND name =:name")
    List<DCategory> checkExisted(String rootId, int type, String name);

    @Query("SELECT * FROM DCategory WHERE rootId IS NULL AND type =:type AND name =:name")
    List<DCategory> checkExisted(int type, String name);

}
