package com.money.data.dao;

import androidx.room.Dao;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.money.data.entity.DTransaction;

import java.util.List;

@Dao
public interface TransactionAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert (DTransaction... transactions);

    @Update
    void update (DTransaction... transactions);

    @Delete
    void delete (DTransaction... transactions);

    @Query("DELETE FROM DTransaction WHERE categoryId = :categoryId")
    void deleteByCategory(String categoryId);

    @Query("SELECT * FROM DTransaction ")
    List<DTransaction> getAll();

}
