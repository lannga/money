package com.money.data.entity;

import androidx.annotation.NonNull;
import androidx.room.DatabaseView;

@DatabaseView(
        "SELECT t.*, c.name AS categoryName, c.icon AS categoryIcon, c.rootId AS rootCategoryId, c.type AS  type" +
                "FROM `Transaction` AS t LEFT JOIN Category AS c " +
                "ON t.categoryId = c.id ")

public class VTransaction extends DTransaction {
    private int categoryIcon;
    private String categoryName;
    private String rootCategoryId;
    private int type;

    public VTransaction(@NonNull String id, String categoryId, int amount, long time, String note, int categoryIcon, String categoryName, String rootCategoryId, int type) {
        super(id, categoryId, amount, time, note);
        this.categoryIcon = categoryIcon;
        this.categoryName = categoryName;
        this.rootCategoryId = rootCategoryId;
        this.type = type;
    }

    public int getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(int categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getRootCategoryId() {
        return rootCategoryId;
    }

    public void setRootCategoryId(String rootCategoryId) {
        this.rootCategoryId = rootCategoryId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
