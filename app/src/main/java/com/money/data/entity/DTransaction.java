package com.money.data.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(
                entity = DCategory.class,
                parentColumns = "id",
                childColumns = "categoryId"
        )
})

public class DTransaction {
    @PrimaryKey
    @NonNull
    private String id;
    @ColumnInfo(index = true)
    private String categoryId;
    private int amount;
    private long time;
    private String note;

    public DTransaction(@NonNull String id, String categoryId, int amount, long time, String note) {
        this.id = id;
        this.categoryId = categoryId;
        this.amount = amount;
        this.time = time;
        this.note = note;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
